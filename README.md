## Consignes pour accéder à la fiche
Cloner le projet GitLab avec la commande
`git clone https://gitlab.com/jdonet/diu-bloc1-fiche`

## Consignes pour commenter la fiche
Pour commenter la fiche, envoyez un mail à l'adresse suivante (laisser les + et tout le reste) :
`incoming+jdonet-diu-bloc1-fiche-13040156-issue-@incoming.gitlab.com`


